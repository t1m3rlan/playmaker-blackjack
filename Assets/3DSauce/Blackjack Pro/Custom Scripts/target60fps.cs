﻿using UnityEngine;
using System.Collections;

public class target60fps : MonoBehaviour
{
    void Awake()
    {
#if UNITY_WEBGL
        //Don't force 60fps in WebGL
#else
        Application.targetFrameRate = 300;
#endif
    }
}