﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory(ActionCategory.Renderer)]
    [Tooltip("Set Sorting Order of Sprite Renderers.")]
    public class SpriteRendererSortingOrders : FsmStateAction
    {
        [RequiredField]
        public FsmGameObject sprite;
        private SpriteRenderer component;
        public FsmInt sortOrder;

        public override void Reset()
        {
            sprite = null;
        }

        public override void OnEnter()
        {
            component = sprite.Value.GetComponent<SpriteRenderer>();
            component.sortingOrder = sortOrder.Value;
            Finish();
        }
    }
}