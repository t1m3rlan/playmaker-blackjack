﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;
using HutongGames.PlayMaker;
using UnityEngine.EventSystems;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory(ActionCategory.Events)]
    [Tooltip("Enable or Disable an Event System component.")]
    public class EnableDisableEventSystem : FsmStateAction
    {
        [RequiredField]
        public FsmGameObject eventSystemObj;
        public bool enable = false;
        private EventSystem component;

        public override void Reset()
        {
            eventSystemObj = null;
            enable = false;
        }

        public override void OnEnter()
        {
            component = eventSystemObj.Value.GetComponent<EventSystem>();

            if (enable)
            {
                component.enabled = true;
            }
            else
            {
                component.enabled = false;
            }

            Finish();
        }
    }
}